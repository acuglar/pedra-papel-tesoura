import './App.css';
import { useState, useEffect } from 'react';
import Placar from './components/Placar';
import Choose from './components/Choose';
import Icons from './components/Icons';

const App = () => {

  const rps = ["rock", "paper", "scissors"];
  const [rock, paper, scissors] = rps;

  const [scoreP, setScoreP] = useState(0);
  const [scoreM, setScoreM] = useState(0);
  const [player, setPlayer] = useState("");
  const [maquina, setMaquina] = useState("");

  useEffect(() => {
    victoryConditions();
  }, [player, maquina])

  const comTurn = () => {
    const random = rps[(Math.floor(Math.random() * rps.length))]
    setMaquina(random)
  }

  const playerTurn = (player) => {
    console.log(player)
    setPlayer(player)
  }

  const victoryConditions = () => {
    if (((player === rock) && (maquina === scissors))
      || ((player === paper) && (maquina === rock))
      || ((player === scissors) && (maquina === paper))) {
      setScoreP(scoreP + 1)
    }
    if (((maquina === rock) && (player === scissors))
      || ((maquina === paper) && (player === rock))
      || ((maquina === scissors) && (player === paper))) {
      setScoreM(scoreM + 1)
    }
  }

  const game = (player) => {
    playerTurn(player);
    comTurn();
  }

  return (
    <div className="App">
      <div className="App-header">
        <div className="Container">
          <h1>jokenpô</h1>
          <div className="Game">
            <Placar scoreP={scoreP} scoreM={scoreM} />
            <Icons player={player} maquina={maquina} rock={rock} paper={paper} scissors={scissors} />
          </div>
          <Choose game={game} rock={rock} paper={paper} scissors={scissors} />
        </div>
      </div>
    </div >
  );
}

export default App;
