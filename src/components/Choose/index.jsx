import { GiPaper } from "react-icons/gi";
import { GiRock } from "react-icons/gi";
import { GiScissors } from "react-icons/gi";
import "./index.css";

const Choose = ({ game, rock, paper, scissors }) => {
  return (
    <div className="btnContainer">
      Select:
      <GiRock className="btn" onClick={() => game(rock)}>
        Pedra
      </GiRock>
      <GiPaper className="btn" onClick={() => game(paper)}>
        Papel
      </GiPaper>
      <GiScissors className="btn" onClick={() => game(scissors)}>
        Tesoura
      </GiScissors>
    </div>
  );
};

export default Choose;
