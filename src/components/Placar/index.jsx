import "./index.css";

const Placar = ({ scoreM, scoreP }) => {
  return (
    <div className="score">
      <div>Jogador : {scoreP}</div>
      <div>{scoreM} : Máquina</div>
    </div>
  );
};

export default Placar;
