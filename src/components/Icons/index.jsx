import { GiPaper } from "react-icons/gi";
import { GiRock } from "react-icons/gi";
import { GiScissors } from "react-icons/gi";
import "./index.css";

const Icons = ({ player, maquina, rock, paper, scissors }) => {
  return (
    <div className="icons">
      <div>
        {player === rock ? (
          <GiRock />
        ) : player === paper ? (
          <GiPaper />
        ) : player === scissors ? (
          <GiScissors />
        ) : (
          "Player"
        )}
      </div>
      <div>
        {maquina === rock ? (
          <GiRock className="comIcons" />
        ) : maquina === paper ? (
          <GiPaper className="comIcons" />
        ) : maquina === scissors ? (
          <GiScissors className="comIcons" />
        ) : (
          "Com"
        )}
      </div>
    </div>
  );
};

export default Icons;
